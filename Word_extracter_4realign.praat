# This script is the first script used for realigning files, which will enable
# the addition of a segmental tier to the TGs.
# The script finds specific words from the word tier of the TG, chops out the
# item and saves it in an individual wav. It also saves a record of extracted
# items.

# This script optionally uses a list of exception words to skip.
# If the script is run with a selected Strings object, this will be used for
# the exceptions. If not, the default `wordlist.csv` file (in the input
# directory) will be read. If that file does not exist, a dialog box will open
# to give the user the chance to select the exception file. If none is chosen,
# no list will be used.

# written by Sonia Granlund 27\02\2015
# adapted by Valerie Hazan 05\10\2016
# adapted by Sonia 18\10\2016

# Ask the user for the input and output directories
form Input Enter directory and output folder name
  sentence Input_directory
  sentence Output_directory
  integer Word_tier 1
endform

@checkDirectory: input_directory$, "Choose input directory..."
input_directory$ = checkDirectory.name$

@checkDirectory: output_directory$, "Choose output directory..."
output_directory$ = checkDirectory.name$

# Reads in list of exceptions
exception_list = numberOfSelected("Strings")
if exception_list
  exception_list = selected("Strings")
else
  exception_file$ = input_directory$ + "/wordlist.csv"
  if !fileReadable(exception_file$)
    @checkFilename: "", "Choose exception list..."
    exception_file$ = checkFilename.name$
  endif

  if exception_file$ != ""
    exception_list = Read Strings from raw text file: exception_file$
  else
    exception_list = 0
  endif
endif

if exception_list
  selectObject: exception_list
  total_exceptions = Get number of strings
endif

# Reads in a list of files by matching patterns
file_list = Create Strings as file list: "textgrids",
  ... input_directory$ + "/*.TextGrid"

# Make txt file for detailed results file
details = Create Table with column names: "details", 0,
  ... "filename interval word startp duration"

# Creates the silence sound file used later (0.1 seconds long)
silence = Create Sound from formula: "SILP", 1, 0, 0.1, 44100, "0"

# Counts number of lines (filenames) in list, opens loop
selectObject: file_list
total_files = Get number of strings

### GO THROUGH ALL TG FILES###
for file to total_files
  file_name$ = Object_'file_list'$[file]
  root_name$ = file_name$ - ".TextGrid"

  # Reads in TG and sound file
  textgrid = Read from file: input_directory$ + "/" + file_name$
  sound    = Read from file: input_directory$ + "/" + root_name$ + ".wav"

  selectObject: textgrid
  # Get the number of intervals for the word tier
  total_intervals = Get number of intervals: word_tier

  # Loop through all the intervals on the word tier
  for interval to total_intervals
    selectObject: textgrid

    # Find label of interval
    word$ = Get label of interval: word_tier, interval

    # Convert to lowercase
    word$ = replace_regex$(word$, ".", "\L&", 0)

    # Valid words do not have flags and are not in exception list
    @is_word_valid: word$
    if word_is_valid
      selectObject: textgrid

      # Get keyword start and end points
      keywstart = Get start point: word_tier, interval
      keywend = Get end point: word_tier, interval

      # Get duration of interval
      kwduration = keywend - keywstart

      # Extract soundfile from between those times
      selectObject: sound
      part = Extract part: keywstart, keywend, "Rectangular", 1, 0

      # Add silence to the wav file; 0.1s before and after
      selectObject: silence
      right_pad = Copy: "pad"
      selectObject: silence, part, right_pad
      padded = Concatenate
      removeObject: right_pad, part

      # Save the padded Sound
      base_name$ = output_directory$ + "/" + root_name$ + "-" + string$(interval)
      Write to WAV file: base_name$ + ".wav"
      removeObject: padded

      # Save to label
      sil$ = "SILP"
      writeFileLine: base_name$ + ".txt", sil$, " ", word$, " ", sil$

      selectObject: details
      Append row
      Set string value:  Object_'details'.nrow, "filename", root_name$
      Set numeric value: Object_'details'.nrow, "interval", interval
      Set string value:  Object_'details'.nrow, "word",     word$
      Set numeric value: Object_'details'.nrow, "startp",   keywstart
      Set numeric value: Object_'details'.nrow, "duration", kwduration
    endif

  endfor

  removeObject: sound, textgrid
endfor

selectObject: details
Save as tab-separated file: output_directory$ + "/detailed.txt"

removeObject: file_list, silence, details
nocheck removeObject: exception_list

procedure is_word_valid: .word$
  .exception = 0
  if index_regex(.word$, "[<>-]")
    .exception = 1
  endif

  .i = 1
  repeat
    if .word$ = Object_'exception_list'$[.i]
      # Count number of times the keyword is found
      .exception = 1
    endif
    .i += 1
  until .exception or .i >= total_exceptions

  word_is_valid = !.exception
endproc

# Taken from the utils CPrAN plugin
# http://cpran.net/plugins/utils
procedure checkFilename (.name$, .label$)
  if .name$ = ""
    .name$ = chooseReadFile$(.label$)
  endif
  # if .name$ = ""
  #   exit
  # endif
endproc

# Taken from the utils CPrAN plugin
# http://cpran.net/plugins/utils
procedure checkDirectory (.name$, .label$)
  if .name$ = ""
    .name$ = chooseDirectory$(.label$)
  endif
  if .name$ = ""
    exit
  endif

  # Copied from `@normaliseDirectory` to limit includes
  .name$ = replace_regex$(.name$, "^~", homeDirectory$, 0)
  .name$ = replace_regex$(.name$, "\\", "/", 0)
  # .name$ = if right$(.name$, 1) != "/" then .name$ + "/" else .name$ fi
endproc
